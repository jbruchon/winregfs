# Makefile for winregfs

PROGRAM_NAME="winregfs"

CC ?= gcc
COMPILER_OPTIONS += -O2 -g
COMPILER_OPTIONS += -std=gnu99 -I. -D_FILE_OFFSET_BITS=64 -pipe -fstrict-aliasing
COMPILER_OPTIONS += -Wall -Wextra -Wwrite-strings -Wcast-align -Wstrict-aliasing -pedantic -Wstrict-overflow -Wstrict-prototypes -Wpointer-arith -Wundef
COMPILER_OPTIONS += -Wshadow -Wfloat-equal -Wstrict-overflow=5 -Waggregate-return -Wcast-qual -Wswitch-default -Wswitch-enum -Wunreachable-code -Wformat=2 -Winit-self
FUSE_CFLAGS = $(shell pkg-config fuse --cflags)
FUSE_LDFLAGS = $(shell pkg-config fuse --libs)
FUSE_LIBS = -lfuse

PREFIX = /usr/local
EXEC_PREFIX = $(PREFIX)
BIN_DIR = $(EXEC_PREFIX)/bin
MAN_DIR = $(PREFIX)/man

OBJS_LIB = ntreg.o
OBJS_FSCK = fsck_winregfs.o $(OBJS_LIB)
OBJS_MOUNT = winregfs.o $(OBJS_LIB)

# Remove unused code if requested
ifdef GC_SECTIONS
COMPILER_OPTIONS += -fdata-sections -ffunction-sections
LINK_OPTIONS += -Wl,--gc-sections
endif

### Find and use nearby libjodycode by default
ifndef IGNORE_NEARBY_JC
 ifneq ("$(wildcard ../libjodycode/libjodycode.h)","")
  $(info Found and using nearby libjodycode at ../libjodycode)
  COMPILER_OPTIONS += -I../libjodycode -L../libjodycode
  ifeq ("$(wildcard ../libjodycode/version.o)","")
   $(error You must build libjodycode before building winregfs)
  endif
 endif
 ifdef FORCE_JC_DLL
  LINK_OPTIONS += -l:../libjodycode/libjodycode.dll
 else
  LINK_OPTIONS += -ljodycode
 endif
endif


CFLAGS += $(COMPILER_OPTIONS) $(CFLAGS_EXTRA)
LDFLAGS += $(LINK_OPTIONS) $(LDFLAGS_EXTRA)

all: libjodycode_hint $(PROGRAM_NAME) dynamic_jc manual

$(PROGRAM_NAME): $(OBJS_MOUNT) $(OBJS_FSCK)

dynamic_jc: $(PROGRAM_NAME)
	$(CC) $(CFLAGS) $(OBJS_MOUNT) -Wl,-Bdynamic $(LDFLAGS) $(FUSE_CFLAGS) $(FUSE_LDFLAGS) -o mount.winregfs $(FUSE_LIBS)
	$(CC) $(CFLAGS) $(OBJS_FSCK) -Wl,-Bdynamic $(LDFLAGS) $(FUSE_CFLAGS) $(FUSE_LDFLAGS) -o fsck.winregfs

static_jc: $(PROGRAM_NAME)
	$(CC) $(CFLAGS) $(OBJS_MOUNT) -Wl,-Bstatic $(LDFLAGS) -Wl,-Bdynamic $(FUSE_CFLAGS) $(FUSE_LDFLAGS) -o mount.winregfs $(FUSE_LIBS)
	$(CC) $(CFLAGS) $(OBJS_FSCK) -Wl,-Bstatic $(LDFLAGS) -Wl,-Bdynamic $(FUSE_CFLAGS) $(FUSE_LDFLAGS) -o fsck.winregfs

manual:
	gzip -9 < mount.winregfs.8 > mount.winregfs.8.gz
	gzip -9 < fsck.winregfs.8 > fsck.winregfs.8.gz

.c.o:
	$(CC) -c $(COMPILER_OPTIONS) $(FUSE_CFLAGS) $(CFLAGS) $<

clean:
	rm -f *.o *~ mount.winregfs fsck.winregfs debug.log *.?.gz

distclean:
	rm -f *.o *~ mount.winregfs fsck.winregfs debug.log *.?.gz winregfs*.pkg.tar.*

install: all
	install -D -o root -g root -m 0644 mount.winregfs.8.gz $(DESTDIR)/$(MAN_DIR)/man8/mount.winregfs.8.gz
	install -D -o root -g root -m 0644 fsck.winregfs.8.gz $(DESTDIR)/$(MAN_DIR)/man8/fsck.winregfs.8.gz
	install -D -o root -g root -m 0755 -s mount.winregfs $(DESTDIR)/$(BIN_DIR)/mount.winregfs
	install -D -o root -g root -m 0755 -s fsck.winregfs $(DESTDIR)/$(BIN_DIR)/fsck.winregfs

package:
	+./chroot_build.sh

libjodycode_hint:
	@echo "hint: if ../libjodycode is built and Make fails, try doing 'make USE_NEARBY_JC=1 static_jc'"$$'\n'
